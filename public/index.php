<?php
require "../bootstrap.php";

$pdo = new PDO(
    "mysql:host={$config["host"]};dbname={$config["database"]};charset={$config["charset"]}",
    $config["login"],
    $config["password"]
);

$announcement = $pdo->query("
SELECT 
announcement_id,
title, 
created_at,
short_description  
FROM ogloszenia.announcement
")->fetchAll(PDO::FETCH_OBJ);


echo $twig->render("index.html.twig", [
    "announcements" => $announcement
]);