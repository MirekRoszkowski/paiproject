<?php
require "../bootstrap.php";

$annoId = filter_input(INPUT_GET, "view", FILTER_VALIDATE_INT);

if ($annoId == NULL) {
    echo $twig->render("view_not_found.html.twig");
    exit;
}

$pdo = new PDO(
    "mysql:host={$config["host"]};dbname={$config["database"]};charset={$config["charset"]}",
    $config["login"],
    $config["password"]
);

$anno = $pdo->prepare("SELECT title, created_at, content  from ogloszenia.announcement where announcement_id = ?");
$anno->execute([
    $annoId
]);
$announ = $anno->fetch(PDO::FETCH_OBJ);

if ($announ === FALSE) {
    echo $twig->render("view_not_found.html.twig");
    exit;
}

echo $twig->render("view.html.twig", [
    "anno" => $announ
]);