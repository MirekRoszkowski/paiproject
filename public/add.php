<?php
require "../bootstrap.php";

$pdo = new PDO(
    "mysql:host={$config["host"]};dbname={$config["database"]};charset={$config["charset"]}",
    $config["login"],
    $config["password"]
);

$title = filter_input(INPUT_POST, 'anno_title');
$short_desc = filter_input(INPUT_POST, 'anno_short');
$content = filter_input(INPUT_POST, 'anno_content');
$currentDate = new DateTime();

if (!empty($title) && !empty($short_desc) && !empty($content)) {
    $pdo->beginTransaction();
    try {

        $sql = "INSERT INTO announcement (title,short_description,content,created_at) VALUES (?, ?, ?, ?)";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([
                $title,
                $short_desc,
                $content,
                $currentDate->format("Y-m-d H:i:s")
            ]
        );
        $pdo->commit();

    } catch (Exception $e) {
        echo($e->getMessage());
        $pdo->rollBack();
        exit;
    }
}
echo $twig->render("add.html.twig",

    [
        'form_action' => $_SERVER["PHP_SELF"],
        'form_method' => "POST"
    ]
);