-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: efik-db:3306
-- Czas generowania: 23 Cze 2018, 09:55
-- Wersja serwera: 10.3.7-MariaDB-1:10.3.7+maria~jessie
-- Wersja PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `ogloszenia`
--
CREATE DATABASE IF NOT EXISTS `ogloszenia` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `ogloszenia`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `announcement`
--

DROP TABLE IF EXISTS `announcement`;
CREATE TABLE `announcement` (
  `announcement_id` int(11) NOT NULL,
  `title` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `announcement`
--

INSERT INTO `announcement` (`announcement_id`, `title`, `created_at`, `content`, `short_description`) VALUES
(1, 'JanuszeX Sp. z. o.o', '2018-06-23 11:01:30', '<h2>Preambuła</h2><p><strong>This is a short tutorial on how to use t</strong>ransactions with PDO object in PHP. For those who don’t know; database transactions represent a “block” or “unit” of work. In most cases, this “unit” of work will consist of multiple queries that are somehow related to one another.&nbsp;In certain scenarios, you might want to ensure that all queries have&nbsp;executed successfully&nbsp;</p><p>&nbsp;</p><p>BEFORE you commit the changes to the database.&nbsp;Example:&nbsp;The second query should not be executed if the first query fails.</p><p>&nbsp;</p><p>For this example, I’m going to create a mock application that adds credit or money to a user’s account:</p>', 'Zatrudnimy niewolnika na pełen etat za 8 zł na godzinę. Zapraszamy!!');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`announcement_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `announcement`
--
ALTER TABLE `announcement`
  MODIFY `announcement_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
