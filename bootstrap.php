<?php
require "vendor/autoload.php";
$loader = new Twig_Loader_Filesystem('../resources/');
$twig = new Twig_Environment($loader);

if(!file_exists(__DIR__."/config.php")){
    die("Rename config.php.dist to config.php and fill config!");
}

$config = require_once "config.php";